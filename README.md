# theory-prog
# Домашнее задание №3 по Технологии Программирования.
## Выполнила: Балануца Дарья БИ2006
### Часть 1.
*Для получения оценки "1"*: Реализовала поиск суммы, произведения, максимума и минимума, считывая данные из текстового файла.
#### Код находится в файле: `tz3.py`

*Для получения оценки "2"*: Реализовала тесты для проверки корректности функций поиска минимума, максимума, сложения и умножения. 
#### Код находится в файле: `test_functions.py`

*Для получения оценки "3"*: Реализовала тесты для проверки скорости работы программы при увеличении размера входного файла. Код составляет два различных массива, которые отличаются количеством элементов и решает ту же задачу на поиск переменных.
#### Код находится в файле: `test_speed.py`

*Для получения оценки "4"*: реализуйте любой другой тест на ваше усмотрение

*Для получения оценки "5"*: Реализовала программу так, чтобы не возникало аварийного завершения работы программы из-за ошибки переполнения. Использовала функцию try, которая при возникновении ошибки сообщает о ней, а не аварийно закрывается.

### Часть 2.
*Для получения оценки "1"*: Завела репозиторий `https://github.com/dariabalanuta/theory-prog`. Оформила простейший *README.md*(https://github.com/dariabalanuta/theory-prog/blob/main/README.md). Загрузила в репозиторий файлы своего мини-проекта (код, тесты, *README.md*).

*Для получения оценки "2"*: подключите к вашему проекту любую CI-систему (**выше есть подсказки с вариантами систем, но мы крайне рекомендуем использовать GitHub Actions в рамках этого задания, только если Ваш семинарист не демонстрировал вам другую систему**). Обеспечьте возможность запуска тестов в ручном режиме (например, по щелчку кнопки в веб-интерфейсе CI-системы) 

**Для получения оценки "3"**: настройте CI таким образом, чтобы прогон тестов запускался автоматически при любом новом коммите в репозиторий вашего проекта

**Для получения оценки "4"**: сделайте интеграцию CI-системы и вашего репозитория на GitHub: сделайте бэйдж в *README.md*, который будет показывать текущий статус тестов. Для информации смотрите [тут](https://docs.github.com/en/actions/monitoring-and-troubleshooting-workflows/adding-a-workflow-status-badge), [тут](https://www.codeblocq.com/2016/04/Add-a-build-passing-badge-to-your-github-repository/) или в аналогичном доке для выбранной вами CI-системы.  Как выглядят бэйджи в целом, можно посмотреть в любом проекте на GitHub, где они сделаны, например, в [репозитории Telegram](https://github.com/telegramdesktop/tdesktop).

**Для получения оценки "5"**: сделайте любую интеграцию CI-системы и какого-либо мессенджера (например, *telegram*, *slack*, *msteams* и т.п.). Настройте систему так, чтобы при успешном прохождении теста посылалось сообщение "все ок", при неуспешном - посылалась информация, какие именно тесты не пройдены
